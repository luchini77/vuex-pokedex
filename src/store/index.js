import { createStore } from 'vuex'

export default createStore({
  state: {
    pokemon: [],
  },
  getters: {
  },
  mutations: {
    setPokemon(state, payload){
      state.pokemon = payload
    }
  },
  actions: {
    async getPokemon({commit}, dato){
      //console.log(dato)
      try {
        const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${dato}/`)
        const data = await res.json()
        console.log(data)
        commit('setPokemon', data)
      } catch (error) {
        console.log(error)
      }
    }
  },
  modules: {
  }
})
